package com.appsdeveloperblog.app.ws.io.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "AppUser")
public class UserEntity implements Serializable
{
	private static final long serialVersionUID = 5313493413859894403L;
	
	@Id
	@GeneratedValue
	private long id;
	
	@Column(nullable=false)
	private String userId;

	@Column(nullable=false, length=50)
	private String firstName;
	
	@Column(nullable=false, length=50)
	private String lastName;
	
	@Column(nullable=false, length=120, unique=true)
	private String email;
	
	@Column(nullable=false)
	private String encryptedPassword;
	
	private String emailVerificationToken;
	
	@Column(nullable=false)
	private Boolean emailVerificationStatus = false;
	
//	@OneToMany(mappedBy="userDetails", cascade=CascadeType.ALL)
//	private List<AddressEntity> addresses;

	
	public String getEmail()
	{
		return email;
	}

	public Boolean getEmailVerificationStatus()
	{
		return emailVerificationStatus;
	}

	public String getEmailVerificationToken()
	{
		return emailVerificationToken;
	}

	public String getEncryptedPassword()
	{
		return encryptedPassword;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public long getId()
	{
		return id;
	}

	public String getLastName()
	{
		return lastName;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setEmail(String theEmail)
	{
		email = theEmail;
	}

	public void setEmailVerificationStatus(Boolean theEmailVerificationStatus)
	{
		emailVerificationStatus = theEmailVerificationStatus;
	}

	public void setEmailVerificationToken(String theEmailVerificationToken)
	{
		emailVerificationToken = theEmailVerificationToken;
	}

	public void setEncryptedPassword(String theEncryptedPassword)
	{
		encryptedPassword = theEncryptedPassword;
	}

	public void setFirstName(String theFirstName)
	{
		firstName = theFirstName;
	}

	public void setId(long theId)
	{
		id = theId;
	}

	public void setLastName(String theLastName)
	{
		lastName = theLastName;
	}

	public void setUserId(String theUserId)
	{
		userId = theUserId;
	}
}
