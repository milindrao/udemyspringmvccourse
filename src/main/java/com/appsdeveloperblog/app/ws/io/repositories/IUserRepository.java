package com.appsdeveloperblog.app.ws.io.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.appsdeveloperblog.app.ws.io.entity.UserEntity;

@Repository
public interface IUserRepository extends CrudRepository<UserEntity, Long>
{
	UserEntity findByEmail(String theEmail);
}
