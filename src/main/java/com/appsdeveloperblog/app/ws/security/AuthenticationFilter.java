package com.appsdeveloperblog.app.ws.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.appsdeveloperblog.app.ws.SpringApplicationContext;
import com.appsdeveloperblog.app.ws.service.IUserService;
import com.appsdeveloperblog.app.ws.shared.dto.UserDTO;
import com.appsdeveloperblog.app.ws.ui.model.request.UserLoginRequestModel;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter
{
	private final AuthenticationManager authenticationManager;
    private String contentType;
    
	public AuthenticationFilter(AuthenticationManager theAuthenticationManager)
	{
		authenticationManager = theAuthenticationManager;
	}
	
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) 
		throws AuthenticationException 
    {
        try {
        	
        	contentType = req.getHeader("Accept");
        	
            UserLoginRequestModel creds = new ObjectMapper().readValue(
        										req.getInputStream(), 
        										UserLoginRequestModel.class);
            
            Authentication theAuthentication =
        		new UsernamePasswordAuthenticationToken(creds.getEmail(),
                            							creds.getPassword(),
                            							new ArrayList<>());            		
            
            return authenticationManager.authenticate(theAuthentication);
            
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
	
    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) 
		throws IOException, ServletException 
    {
        
        String userName = ((User) auth.getPrincipal()).getUsername();  
        
        Date theExpirationDate = 
        	new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME);
        String theTokenSecret = SecurityConstants.TOKEN_SECRET;
        String token = Jwts.builder()
        				   .setSubject(userName)
        				   .setExpiration(theExpirationDate)
        				   .signWith(SignatureAlgorithm.HS512, theTokenSecret)
        				   .compact();

        IUserService userService = 
        	(IUserService) SpringApplicationContext.getBean("userService");
        UserDTO userDTO = userService.getUser(userName);
        
        res.addHeader(SecurityConstants.HEADER_STRING, 
        			  SecurityConstants.TOKEN_PREFIX + token);
        res.addHeader("UserID", userDTO.getUserId());
    }  

}
