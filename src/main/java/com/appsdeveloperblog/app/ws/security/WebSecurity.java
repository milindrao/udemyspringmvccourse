package com.appsdeveloperblog.app.ws.security;

import javax.servlet.Filter;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.appsdeveloperblog.app.ws.service.IUserService;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter
{
	private final IUserService iUserService;
	private final BCryptPasswordEncoder iBCryptPasswordEncoder;
	
	public WebSecurity(IUserService userDetailsService,
			BCryptPasswordEncoder bCryptPasswordEncoder)
	{
		iUserService = userDetailsService;
		iBCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		Filter theAuthenticationFilter =
			new AuthenticationFilter(authenticationManager());
				
		http.csrf()
			.disable()
			.authorizeRequests()
			.antMatchers(HttpMethod.POST, SecurityConstants.SIGN_UP_URL).permitAll()
			.anyRequest().authenticated()
			.and().addFilter(theAuthenticationFilter);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) 
		throws Exception
	{
		auth.userDetailsService(iUserService)
			.passwordEncoder(iBCryptPasswordEncoder);
	}
}
