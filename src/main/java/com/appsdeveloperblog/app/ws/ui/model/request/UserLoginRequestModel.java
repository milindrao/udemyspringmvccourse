package com.appsdeveloperblog.app.ws.ui.model.request;

public class UserLoginRequestModel
{
	private String email;
	private String password;
	
	
	public String getEmail()
	{
		return email;
	}
	public String getPassword()
	{
		return password;
	}
	public void setEmail(String theEmail)
	{
		email = theEmail;
	}
	public void setPassword(String thePassword)
	{
		password = thePassword;
	}
}
