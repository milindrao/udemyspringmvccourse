package com.appsdeveloperblog.app.ws.ui.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.appsdeveloperblog.app.ws.service.IUserService;
import com.appsdeveloperblog.app.ws.shared.dto.UserDTO;
import com.appsdeveloperblog.app.ws.ui.model.request.UserRequestDetailsModel;
import com.appsdeveloperblog.app.ws.ui.model.response.UserRest;

@RestController
@RequestMapping("users")	// http://localhost:8080/users
public class UserController
{
	@Autowired
	IUserService iUserService;
	
	
	@GetMapping
	public String getUser()
	{
		return "getUser was called";
	}
	

	@PostMapping
	public UserRest createUser(@RequestBody UserRequestDetailsModel theUserRequestDetails)
	{
		UserRest theUserRest = new UserRest();
		
		UserDTO theUserDto = new UserDTO(); 
		BeanUtils.copyProperties(theUserRequestDetails, theUserDto);
		
		UserDTO theCreatedUser = iUserService.createUser(theUserDto);
		BeanUtils.copyProperties(theCreatedUser, theUserRest);
		
		return theUserRest;
	}
	
	
	@PutMapping
	public String updateUser()
	{
		return "updateUser was called";
	}
	
	
	@DeleteMapping
	public String deleteUser()
	{
		return "deleteUser was called";
	}
}
