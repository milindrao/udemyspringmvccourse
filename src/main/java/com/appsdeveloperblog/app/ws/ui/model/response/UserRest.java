package com.appsdeveloperblog.app.ws.ui.model.response;

public class UserRest
{
	private String userId;
	private String firstName;
	private String lastName;
	private String email;
	public String getEmail()
	{
		return email;
	}
	public String getFirstName()
	{
		return firstName;
	}
	public String getLastName()
	{
		return lastName;
	}
	public String getUserId()
	
	{
		return userId;
	}
	public void setEmail(String theEmail)
	{
		email = theEmail;
	}
	public void setFirstName(String theFirstName)
	{
		firstName = theFirstName;
	}
	public void setLastName(String theLastName)
	{
		lastName = theLastName;
	}
	public void setUserId(String theUserId)
	{
		userId = theUserId;
	}
}
