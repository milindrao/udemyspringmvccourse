package com.appsdeveloperblog.app.ws.ui.model.request;

public class UserRequestDetailsModel
{
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	
	public String getEmail()
	{
		return email;
	}
	public String getFirstName()
	{
		return firstName;
	}
	public String getLastName()
	{
		return lastName;
	}
	public String getPassword()
	{
		return password;
	}
	public void setEmail(String theEmail)
	{
		email = theEmail;
	}
	public void setFirstName(String theFirstName)
	{
		firstName = theFirstName;
	}
	public void setLastName(String theLastName)
	{
		lastName = theLastName;
	}
	public void setPassword(String thePassword)
	{
		password = thePassword;
	}
}
