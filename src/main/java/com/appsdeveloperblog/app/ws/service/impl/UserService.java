package com.appsdeveloperblog.app.ws.service.impl;

import java.util.ArrayList;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.appsdeveloperblog.app.ws.io.entity.UserEntity;
import com.appsdeveloperblog.app.ws.io.repositories.IUserRepository;
import com.appsdeveloperblog.app.ws.service.IUserService;
import com.appsdeveloperblog.app.ws.shared.dto.UserDTO;
import com.appsdeveloperblog.app.ws.shared.dto.Utils;

@Service
public class UserService implements IUserService
{
	@Autowired
	IUserRepository iUserRepository; 

	@Autowired
	Utils iUtils; 
	
	@Autowired
	BCryptPasswordEncoder iBCryptPasswordEncoder;


	@Override
	public UserDTO createUser(UserDTO theUserDTO)
	{
		UserDTO theReturnedUserDTO = new UserDTO();

		UserEntity theStoredUserEntity = 
			iUserRepository.findByEmail(theUserDTO.getEmail());
		
		if (theStoredUserEntity == null) {
		
			UserEntity theUserEntity = new UserEntity();
			BeanUtils.copyProperties(theUserDTO, theUserEntity);
			
			String theEncodedPassword = 
				iBCryptPasswordEncoder.encode(theUserDTO.getPassword());
			theUserEntity.setEncryptedPassword(theEncodedPassword);
			String theUserId = iUtils.generateUserId(30);
			theUserEntity.setUserId(theUserId);
			
			theStoredUserEntity = iUserRepository.save(theUserEntity);
			
			BeanUtils.copyProperties(theStoredUserEntity, theReturnedUserDTO);
		
		} else {
			String msg = "A record already exists with the email '" + 
						 theUserDTO.getEmail() + "'";
			throw new RuntimeException(msg);
		}
		
		return theReturnedUserDTO;
	}


	@Override
	public UserDTO getUser(String theEmail)
	{
		UserDTO theUserDTO = new UserDTO();
		
		UserEntity theUserEntity = iUserRepository.findByEmail(theEmail);
		if (theUserEntity != null) {
			BeanUtils.copyProperties(theUserEntity, theUserDTO);
		} else {
			String msg = "User with email '" + "' not found in the database"; 
			throw new UsernameNotFoundException(msg);
		}
		
		return theUserDTO;
	}
	
	
	@Override
	public UserDetails loadUserByUsername(String theEmail)
		throws UsernameNotFoundException
	{
		User theUser = null; 
				
		UserEntity theUserEntity = iUserRepository.findByEmail(theEmail);
		if (theUserEntity != null) {
			theUser = new User(theUserEntity.getEmail(),
							   theUserEntity.getEncryptedPassword(),
							   new ArrayList<>());
		
		} else {
			String msg = "User with email '" + "' not found in the database"; 
			throw new UsernameNotFoundException(msg);
		}
		
		return theUser;
	}

}
