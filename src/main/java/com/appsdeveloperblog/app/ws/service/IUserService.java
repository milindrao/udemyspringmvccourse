package com.appsdeveloperblog.app.ws.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.appsdeveloperblog.app.ws.shared.dto.UserDTO;

public interface IUserService extends UserDetailsService
{
	public UserDTO createUser(UserDTO theUser);
	public UserDTO getUser(String email);
}
