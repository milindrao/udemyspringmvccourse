package com.appsdeveloperblog.app.ws.shared.dto;

import java.io.Serializable;

public class UserDTO implements Serializable
{
	private static final long serialVersionUID = 6835192601898364280L;
	private long id;
    private String userId;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String encryptedPassword;
    private String emailVerificationToken;
    private Boolean emailVerificationStatus = false;
    //private List<AddressDTO> addresses;
    
    
	public long getId()
	{
		return id;
	}
	public void setId(long theId)
	{
		id = theId;
	}
	public String getUserId()
	{
		return userId;
	}
	public void setUserId(String theUserId)
	{
		userId = theUserId;
	}
	public String getFirstName()
	{
		return firstName;
	}
	public void setFirstName(String theFirstName)
	{
		firstName = theFirstName;
	}
	public String getLastName()
	{
		return lastName;
	}
	public void setLastName(String theLastName)
	{
		lastName = theLastName;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String theEmail)
	{
		email = theEmail;
	}
	public String getPassword()
	{
		return password;
	}
	public void setPassword(String thePassword)
	{
		password = thePassword;
	}
	public String getEncryptedPassword()
	{
		return encryptedPassword;
	}
	public void setEncryptedPassword(String theEncryptedPassword)
	{
		encryptedPassword = theEncryptedPassword;
	}
	public String getEmailVerificationToken()
	{
		return emailVerificationToken;
	}
	public void setEmailVerificationToken(String theEmailVerificationToken)
	{
		emailVerificationToken = theEmailVerificationToken;
	}
	public Boolean getEmailVerificationStatus()
	{
		return emailVerificationStatus;
	}
	public void setEmailVerificationStatus(Boolean theEmailVerificationStatus)
	{
		emailVerificationStatus = theEmailVerificationStatus;
	}
    
    
}
